package com.sdacademy.spring_start.worker;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@FieldDefaults(level = AccessLevel.PRIVATE)
//@Component
public class CommandLineRunnerWithFieldInjection implements CommandLineRunner {

    @Autowired
    DummyLogger dummyLogger;

    @Override
    public void run(final String... args) throws Exception {
        dummyLogger.sayHello();
    }
}