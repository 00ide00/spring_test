package com.sdacademy.spring_start.task3.worker;

import com.sdacademy.spring_start.task3.log.DummyLogger;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Component
public class WithSecondaryCommandLineRunner implements CommandLineRunner {

    DummyLogger dummyLogger;

    public WithSecondaryCommandLineRunner(@Qualifier("dummyLoggerImpl") DummyLogger dummyLogger) {
        this.dummyLogger = dummyLogger;
    }



    @Override
    public void run(String... args) throws Exception {
        dummyLogger.sayHello();
    }
}
