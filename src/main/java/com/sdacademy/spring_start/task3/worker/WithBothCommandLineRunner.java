package com.sdacademy.spring_start.task3.worker;

import com.sdacademy.spring_start.task3.log.DummyLogger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WithBothCommandLineRunner implements CommandLineRunner {

    private final List<DummyLogger> dummyLoggers;

    public WithBothCommandLineRunner(final List<DummyLogger> dummyLoggers) {
        this.dummyLoggers = dummyLoggers;
    }

    @Override
    public void run(String... args) throws Exception {
        dummyLoggers.forEach(DummyLogger::sayHello);

    }
}
