package com.sdacademy.spring_start.task3.log;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DummyLoggerImpl implements DummyLogger {
    @Override
    public void sayHello() {
        log.info("Hello from DummyLoggerImpl");
    }
}
