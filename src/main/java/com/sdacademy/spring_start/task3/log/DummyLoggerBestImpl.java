package com.sdacademy.spring_start.task3.log;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Slf4j
@Primary
@Component
public class DummyLoggerBestImpl implements DummyLogger {
    @Override
    public void sayHello() {
        log.info("Hello from DummyLoggerBestImpl");
    }
}
