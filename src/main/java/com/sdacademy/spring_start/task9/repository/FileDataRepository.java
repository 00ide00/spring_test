package com.sdacademy.spring_start.task9.repository;

import com.sdacademy.spring_start.task9.entity.FileData;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface FileDataRepository extends CrudRepository<FileData, UUID> {
    List<FileData> findAll();
}
