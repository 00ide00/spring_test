package com.sdacademy.spring_start.task9.controller;

import com.sdacademy.spring_start.task9.dto.FileDataDto;
import com.sdacademy.spring_start.task9.entity.FileData;
import com.sdacademy.spring_start.task9.service.FileDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import static com.sdacademy.spring_start.task9.controller.FileDataController.BASE_URL;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequiredArgsConstructor
@RequestMapping(BASE_URL)
public class FileDataController {
    static final String BASE_URL = "/api/files-data";
    private final FileDataService fileDataService;

    @GetMapping
    public List<FileDataDto> getAllFileData() {
        return fileDataService.getAll();
    }

    @GetMapping(path = "/{id}")
    public FileDataDto getById(@PathVariable UUID id) {
        return fileDataService.getById(id);
    }

    @PostMapping
    public ResponseEntity<FileDataDto> save(@RequestBody FileData fileData) throws URISyntaxException {
        FileDataDto createdFileData = fileDataService.save(fileData);
        URI uri = new URI(BASE_URL + "/" + createdFileData.getId());
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(path = "/{id}")
    @ResponseStatus(NO_CONTENT)
    public void updateFileData(@PathVariable UUID id, @RequestBody FileData fileData) {
        fileDataService.update(fileData, id);
    }
    @DeleteMapping(path = "{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteFileData(@PathVariable UUID id){
        fileDataService.delete(id);
    }
}
