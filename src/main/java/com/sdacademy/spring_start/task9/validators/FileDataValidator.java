package com.sdacademy.spring_start.task9.validators;

import com.sdacademy.spring_start.task9.exceptions.SdaException;
import com.sdacademy.spring_start.task9.repository.FileDataRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Slf4j
@RequiredArgsConstructor
public class FileDataValidator {
    private final FileDataRepository fileDataRepository;

    public void validateFileDataCanBeUpdated(final UUID id) {
        if (!fileDataRepository.existsById(id)) {
            log.warn("{} does not exist in the database.", id);
            throw new SdaException(id + " was not found");
        }
    }

}
