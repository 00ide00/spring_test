package com.sdacademy.spring_start.task9.exceptions;

public class SdaException extends RuntimeException {
    public SdaException(final String message) {
        super(message);
    }
}