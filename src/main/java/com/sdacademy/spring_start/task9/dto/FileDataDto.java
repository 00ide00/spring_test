package com.sdacademy.spring_start.task9.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@FieldDefaults(level = PRIVATE)
@Getter
@Setter
@NoArgsConstructor
public class FileDataDto {

    String id;
    String fileName;
    String extension;
    int sizeInKb;
    String content;

}
