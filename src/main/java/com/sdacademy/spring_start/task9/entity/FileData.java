package com.sdacademy.spring_start.task9.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;
import static lombok.AccessLevel.PRIVATE;

@Data
@NoArgsConstructor
@Entity(name = "files_data")
@FieldDefaults(level = PRIVATE)
public class FileData {

    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(columnDefinition = "uuid")
    UUID id;
    @Column
    String fileName;
    @Column
    String extension;
    @Column
    int sizeInKb;
    @Column
    String content;

}
