package com.sdacademy.spring_start.task9.service;

import com.sdacademy.spring_start.task9.dto.FileDataDto;
import com.sdacademy.spring_start.task9.entity.FileData;
import com.sdacademy.spring_start.task9.exceptions.SdaException;
import com.sdacademy.spring_start.task9.repository.FileDataRepository;
import com.sdacademy.spring_start.task9.validators.FileDataValidator;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class FileDataService {
    FileDataRepository fileDataRepository;
    FileDataValidator fileDataValidator;
    ModelMapper modelMapper;

    public List<FileDataDto> getAll() {
        return fileDataRepository.findAll().stream()
                .map(fileData -> modelMapper.map(fileData, FileDataDto.class))
                .collect(toList());
    }

    public FileDataDto getById(final UUID uuid) {
        val fileData = fileDataRepository.findById(uuid).orElseThrow(() -> new SdaException(uuid + " was not found"));
        return modelMapper.map(fileData, FileDataDto.class);
    }

    public FileDataDto save(final FileData fileData) {
        val savedFileData = fileDataRepository.save(fileData);
        return modelMapper.map(savedFileData, FileDataDto.class);
    }

    public void update(final FileData fileData, final UUID id) {
        fileDataValidator.validateFileDataCanBeUpdated(id);
        fileDataRepository.save(fileData);
    }

    public void delete(final UUID id) {
        fileDataValidator.validateFileDataCanBeUpdated(id);
        fileDataRepository.deleteById(id);
    }
}
