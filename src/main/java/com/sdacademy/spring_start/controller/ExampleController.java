package com.sdacademy.spring_start.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class ExampleController {

    @GetMapping(path = "/test")
    public String exampleGetMapping(@RequestParam String string){

        return string;
    }

    @GetMapping(path = "/simple")
    public String simpleGet(){

        return "simple get";
    }
    @GetMapping(path = "/optional")
    public String optionalParam(@RequestParam Optional<String> string ){
        if (string.isEmpty()){
            return  "no param";
        }
        return string.get();
    }
    @GetMapping(path = "/default")
    public String defaultValue(@RequestParam(defaultValue = "default") String optional){
        return optional;
    }
}
