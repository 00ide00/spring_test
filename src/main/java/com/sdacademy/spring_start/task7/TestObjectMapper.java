package com.sdacademy.spring_start.task7;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class TestObjectMapper implements CommandLineRunner {

    final ObjectMapper objectMapper;

    @Override
    public void run(String... args) throws Exception {
        log.info("Object mapper naming strategy is: ");
        // log.info(objectMapper.getPropertyNamingStrategy().toString());
    }

}
