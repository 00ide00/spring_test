package com.sdacademy.spring_start.task7.controller;

import com.sdacademy.spring_start.task7.model.NamedPairs;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class NamedPairsController {

    @GetMapping(path = "/api/pairs/{name}")
    public NamedPairs getNamedPairs (@PathVariable String name) {
        return NamedPairs.builder().name(name).pairs(Map.of()).build();
    }

}
