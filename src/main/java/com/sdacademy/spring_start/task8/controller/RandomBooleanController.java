package com.sdacademy.spring_start.task8.controller;

import com.sdacademy.spring_start.task8.providers.RandomBooleanProvider;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class RandomBooleanController {

    final private RandomBooleanProvider randomBooleanProvider;

    @GetMapping(path = "/api/random-boolean")
    public boolean getRandomBoolean () {
        return randomBooleanProvider.getValue();
    }

}
