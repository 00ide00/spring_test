package com.sdacademy.spring_start.task4.configs;

import com.sdacademy.spring_start.task4.utils.DummyLogger;
import com.sdacademy.spring_start.task4.utils.ListUtil;
import com.sdacademy.spring_start.task4.utils.StringUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UtilConfiguration {

    @Bean
    public DummyLogger dummyLogger() {

        return new DummyLogger();
    }

    @Bean
    public ListUtil listUtil() {

        return new ListUtil();
    }

    @Bean(name = "stringUtility")
    public StringUtil stringUtil() {

        return new StringUtil();
    }
}
