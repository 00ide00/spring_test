package com.sdacademy.spring_start.task4.service;

import com.sdacademy.spring_start.task4.utils.DummyLogger;
import com.sdacademy.spring_start.task4.utils.ListUtil;
import com.sdacademy.spring_start.task4.utils.StringUtil;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import static java.lang.String.valueOf;
import static java.util.List.of;

@Slf4j
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Service
public class UtilService implements CommandLineRunner {
    DummyLogger dummyLogger;
    ListUtil listUtil;
    StringUtil stringUtil;


    @Override
    public void run(String... args) throws Exception {
        val listOfInt = of(3, 4, 5);
        val listOfString = of("a", "b", "c");
        dummyLogger.sayHi();
        log.info(valueOf(listUtil.sumElements(listOfInt)));
        log.info(stringUtil.formSentence(listOfString));
    }
}
