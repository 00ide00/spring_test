package com.sdacademy.spring_start.task4.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DummyLogger {
    public void sayHi() {
        log.info("Hi from zadanie4");
    }

}

