package com.sdacademy.spring_start.thymeleaf_example;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {

    @GetMapping("/hello")
    public String showHello(final ModelMap modelMap) {
//      TODO finish implementation
        modelMap.addAttribute("attributeOne", "Text for attribute one");
        return "welcome";
    }
}

