package com.sdacademy.spring_start.task11.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PCGameForm {
    private String title;
    private String producer;
    private String genre;
    private Integer minimumAge;
    private Boolean isAAA;

    public String isAAAMessage() {
        return isAAA ? "is AAA" : "is not AAA";
    }
}