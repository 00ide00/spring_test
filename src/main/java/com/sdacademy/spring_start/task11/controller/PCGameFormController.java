package com.sdacademy.spring_start.task11.controller;

import com.sdacademy.spring_start.task11.dao.PCGameForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/pc-games")
public class PCGameFormController {

    @GetMapping
    public String pcGame(final ModelMap modelMap) {
        modelMap.addAttribute("createMessage", "Create PC game");
        modelMap.addAttribute("pcGameForm", new PCGameForm());
        return "pcgame";
    }

    @PostMapping
    public String showPCGame(@ModelAttribute("pcgameform") final PCGameForm pcGameForm, final ModelMap modelMap) {
        modelMap.addAttribute("pcgameform", pcGameForm);
        return "pcgame_info";
    }
}
