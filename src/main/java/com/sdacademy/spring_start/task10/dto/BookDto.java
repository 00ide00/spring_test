package com.sdacademy.spring_start.task10.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@FieldDefaults(level = PRIVATE)
@Getter
@Setter
@NoArgsConstructor
public class BookDto {
    Long id;
    String title;
    String author;
    String ISBN;
    int pagesNum;
}
