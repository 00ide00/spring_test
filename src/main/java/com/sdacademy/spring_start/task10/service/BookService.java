package com.sdacademy.spring_start.task10.service;

import com.sdacademy.spring_start.task10.dto.BookDto;
import com.sdacademy.spring_start.task10.entity.Book;
import com.sdacademy.spring_start.task10.repository.BookRepository;
import com.sdacademy.spring_start.task9.exceptions.SdaException;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.modelmapper.ModelMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PRIVATE;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class BookService {
    BookRepository bookRepository;
    ModelMapper modelMapper;

    public List<BookDto> getAllByTitle(final String title) {
        return getBookDtos(bookRepository.findAllByTitle(title));
    }

    public BookDto getByISBN(final String ISBN) {
        val book = bookRepository.findByISBN(ISBN).orElseThrow(() -> new SdaException(ISBN + " was not found"));
        return modelMapper.map(book, BookDto.class);
    }

    public BookDto getByAuthorAndISBN(final String author,final String ISBN){
        val book = bookRepository.findByAuthorAndISBN(author, ISBN).orElseThrow(() -> new SdaException(author + " with the following ISBN: " + ISBN + " was not found"));
        return modelMapper.map(book, BookDto.class);
    }

    public List<BookDto> getTop3ByAuthorOrderByPagesNumDesc(final String author){
        return getBookDtos(bookRepository.findTop3ByAuthorOrderByPagesNumDesc(author));
    }

    public List<BookDto> getByTitleStartingWith(final String title){
       return getBookDtos(bookRepository.findByTitleStartingWith(title));
    }

    public List<BookDto> getAllByPagesNumBetween(final int min,final int max){
        return getBookDtos((bookRepository.findAllByPagesNumBetween(min, max)));
    }

    public List<BookDto> getWherePagesNumIsGreaterThanX(Integer min){
        return getBookDtos(bookRepository.findWherePagesNumIsGreaterThanX(min));
    }

    private List<BookDto> getBookDtos(final List<Book> books) {
        return books.stream()
                .map(book -> modelMapper.map(book, BookDto.class))
                .collect((toList()));
    }
}
