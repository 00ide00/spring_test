package com.sdacademy.spring_start.task6;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Validated
@Component
@ConfigurationProperties(prefix = "pl.sdacademy.zad6")
public class ConfigurationPropertiesExample {
    @Email(message = "email not valid")
    @NotNull(message = "email is null")
    String email;
    String firstName;
    @NotNull(message = "Your last name is null")
    @Length(min = 3, max = 20)
    String lastName;
    String address;
    @NotNull(message = "age is null")
    @Min(value = 18, message = "You must have at least 18 years")
    int age;
    @NotEmpty(message = "values is empty")
    List<String> values;
    @NotEmpty(message = "attributes is empty")
    Map<String, String> customAttributes;

    @AssertTrue(message = "your address is invalid")
    public boolean isAddressValid() {
        return address != null && address.split(" ").length == 2;
    }

}
