package com.sdacademy.spring_start.task11.dao;

import lombok.val;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PCGameFormTest {
    PCGameForm pcGameForm = new PCGameForm();

    @Test
    void isAAAMessageTrue() {
        // given
        pcGameForm.setIsAAA(true);

        // when
        val message = pcGameForm.isAAAMessage();

        // then
        assertEquals(message, "is AAA");
    }

    @Test
    void isAAAMessageFalse() {
        // given
        pcGameForm.setIsAAA(false);

        // when
        val message = pcGameForm.isAAAMessage();

        // then
        assertEquals(message, "is not AAA");
    }

}
