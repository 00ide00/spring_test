package com.sdacademy.spring_start.task11.controller;

import com.sdacademy.spring_start.task11.dao.PCGameForm;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.ModelMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class PCGameFormControllerTest {

    @Mock
    ModelMap modelMap;

    @Spy
    PCGameForm pcGameForm;

    PCGameFormController pcGameFormController = new PCGameFormController();

    @Test
    void pcGameSuccessful() {
        // when
        val template = pcGameFormController.pcGame(modelMap);

        // then
        assertEquals(template, "pcgame");
        verify(modelMap, times(2)).addAttribute(
                argThat((argument) -> argument.equals("createMessage") || argument.equals("pcGameForm")),
                argThat((argument) -> argument.equals("Create PC game") || argument.getClass().isAssignableFrom(PCGameForm.class))
        );
    }

    @Test
    void showPcGameSuccessful() {
        // when
        val template = pcGameFormController.showPCGame(pcGameForm, modelMap);

        // then
        assertEquals(template, "pcgame_info");
        verify(modelMap, times(1))
                .addAttribute("pcgameform", pcGameForm);
    }
}
