package com.sdacademy.spring_start.task9.service;

import com.sdacademy.spring_start.task9.entity.FileData;
import com.sdacademy.spring_start.task9.exceptions.SdaException;
import com.sdacademy.spring_start.task9.repository.FileDataRepository;
import com.sdacademy.spring_start.task9.validators.FileDataValidator;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Optional;
import java.util.UUID;

import static java.util.List.of;
import static java.util.Optional.empty;
import static java.util.UUID.randomUUID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FileDataServiceTest {

    @Mock
    FileDataRepository fileDataRepository;
    @Mock
    FileDataValidator fileDataValidator;
    @InjectMocks
    FileDataService fileDataService;

    @Spy
    ModelMapper modelMapper;

    @Test
    void getAllSuccessful() {
        // given
        val fileData = new FileData();
        fileData.setFileName("file name");
        fileData.setContent("file content");
        fileData.setExtension("jpg");
        fileData.setSizeInKb(50);
        val secondFileData = new FileData();
        secondFileData.setFileName("second file name");
        secondFileData.setContent("second file content");
        secondFileData.setExtension("jpg");
        secondFileData.setSizeInKb(50);
        val fileDataList = of(fileData, secondFileData);
        when(fileDataRepository.findAll()).thenReturn(fileDataList);

        // when
        val receivedFileDataList = fileDataService.getAll();

        // then
        assertEquals(fileDataList.get(0).getId(), receivedFileDataList.get(0).getId());
        assertEquals(fileDataList.get(0).getFileName(), receivedFileDataList.get(0).getFileName());
        assertEquals(fileDataList.get(0).getContent(), receivedFileDataList.get(0).getContent());
        assertEquals(fileDataList.get(0).getExtension(), receivedFileDataList.get(0).getExtension());
        assertEquals(fileDataList.get(0).getSizeInKb(), receivedFileDataList.get(0).getSizeInKb());
        assertEquals(fileDataList.get(1).getId(), receivedFileDataList.get(1).getId());
        assertEquals(fileDataList.get(1).getFileName(), receivedFileDataList.get(1).getFileName());
        assertEquals(fileDataList.get(1).getContent(), receivedFileDataList.get(1).getContent());
        assertEquals(fileDataList.get(1).getExtension(), receivedFileDataList.get(1).getExtension());
        assertEquals(fileDataList.get(1).getSizeInKb(), receivedFileDataList.get(1).getSizeInKb());
    }

    @Test
    void getByIdSuccessful() {
        // given
        val id = randomUUID();
        val fileData = new FileData();
        fileData.setFileName("file name");
        fileData.setContent("file content");
        fileData.setExtension("jpg");
        fileData.setSizeInKb(50);
        val optionalFileData = Optional.of(fileData);
        when(fileDataRepository.findById(id)).thenReturn(optionalFileData);

        // when
        val receivedFileData = fileDataService.getById(id);

        // then
        assertEquals(receivedFileData.getId(), fileData.getId());
        assertEquals(receivedFileData.getFileName(), fileData.getFileName());
        assertEquals(receivedFileData.getContent(), fileData.getContent());
        assertEquals(receivedFileData.getExtension(), fileData.getExtension());
        assertEquals(receivedFileData.getSizeInKb(), fileData.getSizeInKb());
    }

    @Test
    void getByIdFailedWithSdaException() {
        // given
        val id = randomUUID();
        Optional<FileData> nullOptional = empty();
        when(fileDataRepository.findById(id)).thenReturn(nullOptional);

        //when
        val sdaException = assertThrows(
                SdaException.class,
                () -> fileDataService.getById(id)
        );

        // then
        assertEquals(sdaException.getMessage(), id + " was not found");
    }

    @Test
    void saveSuccessful() {
        // given
        val fileData = new FileData();
        fileData.setFileName("file name");
        fileData.setContent("file content");
        fileData.setExtension("jpg");
        fileData.setSizeInKb(50);
        when(fileDataRepository.save(fileData)).thenReturn(fileData);

        // when
        val receivedFileData = fileDataService.save(fileData);

        // then
        assertEquals(receivedFileData.getId(), fileData.getId());
        assertEquals(receivedFileData.getFileName(), fileData.getFileName());
        assertEquals(receivedFileData.getContent(), fileData.getContent());
        assertEquals(receivedFileData.getExtension(), fileData.getExtension());
        assertEquals(receivedFileData.getSizeInKb(), fileData.getSizeInKb());
    }

    @Test
    void updateSuccessful() {
        // given
        val fileData = new FileData();
        val id = UUID.randomUUID();
        fileData.setFileName("file name");
        fileData.setContent("file content");
        fileData.setExtension("jpg");
        fileData.setSizeInKb(50);

        // when
        fileDataService.update(fileData, id);

        // then
        verify(fileDataRepository, times(1)).save(fileData);
        verify(fileDataValidator, times(1)).validateFileDataCanBeUpdated(id);
    }

    @Test
    void deleteSuccessful() {
        // given
        val id = UUID.randomUUID();

        // when
        fileDataService.delete(id);

        // then
        verify(fileDataRepository, times(1)).deleteById(id);
        verify(fileDataValidator, times(1)).validateFileDataCanBeUpdated(id);
    }
}