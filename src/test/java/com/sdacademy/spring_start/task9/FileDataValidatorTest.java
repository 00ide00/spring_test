package com.sdacademy.spring_start.task9;

import com.sdacademy.spring_start.task9.exceptions.SdaException;
import com.sdacademy.spring_start.task9.repository.FileDataRepository;
import com.sdacademy.spring_start.task9.validators.FileDataValidator;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FileDataValidatorTest {

    @Mock
    FileDataRepository fileDataRepository;

    @InjectMocks
    FileDataValidator fileDataValidator;

    @Test
    public void validateFileDataCanBeUpdatedSuccessful() {
        // given
        val id = UUID.randomUUID();
        when(fileDataRepository.existsById(id)).thenReturn(true);

        // then
        try {
            fileDataValidator.validateFileDataCanBeUpdated(id);
            verify(fileDataRepository, times(1)).existsById(id);
        } catch (SdaException e) {
            fail("no errors should be thrown");
        }
    }
    @Test
    public void validateFileDataCanBeUpdateFailedWithSdaException(){
        // given
        val id = UUID.randomUUID();
        when(fileDataRepository.existsById(id)).thenReturn(false);

        // when
        val sdaException = assertThrows(
                SdaException.class,
                () -> fileDataValidator.validateFileDataCanBeUpdated(id)
        );

        // then
        assertEquals(sdaException.getMessage(), id + " was not found");
    }
}
