package com.sdacademy.spring_start.task9.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sdacademy.spring_start.SpringStartApplication;
import com.sdacademy.spring_start.task9.entity.FileData;
import com.sdacademy.spring_start.task9.repository.FileDataRepository;
import lombok.SneakyThrows;
import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@SpringBootTest(classes = SpringStartApplication.class)
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(PER_CLASS)
public class FileDataControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    FileDataRepository fileDataRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @SneakyThrows
    @DisplayName(
            "Should return 'OK' when fetching all file data"
    )
    void getAllFileDataSuccessful() {
        // given
        val fileData = new FileData();
        fileData.setFileName("file name");
        fileData.setContent("file content");
        fileData.setExtension("jpg");
        fileData.setSizeInKb(50);
        fileDataRepository.save(fileData);
        val secondFileData = new FileData();
        secondFileData.setFileName("second file name");
        secondFileData.setContent("second file content");
        secondFileData.setExtension("jpg");
        secondFileData.setSizeInKb(50);
        fileDataRepository.save(secondFileData);

        mockMvc.perform(get("/api/files-data"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].fileName", is(fileData.getFileName())))
                .andExpect(jsonPath("$[1].fileName", is(secondFileData.getFileName())))
                .andExpect(jsonPath("$[0].content", is(fileData.getContent())))
                .andExpect(jsonPath("$[1].content", is(secondFileData.getContent())))
                .andExpect(jsonPath("$[0].extension", is(fileData.getExtension())))
                .andExpect(jsonPath("$[1].extension", is(secondFileData.getExtension())))
                .andExpect(jsonPath("$[0].sizeInKb", is(fileData.getSizeInKb())))
                .andExpect(jsonPath("$[1].sizeInKb", is(secondFileData.getSizeInKb())));
    }

    @Test
    @SneakyThrows
    @DisplayName(
            "Should return 'OK' when fetching by id"
    )
    void getByIdSuccessful() {
        // given
        val fileData = new FileData();
        fileData.setFileName("file name");
        fileData.setContent("file content");
        fileData.setExtension("jpg");
        fileData.setSizeInKb(50);
        val id = fileDataRepository.save(fileData).getId();

        mockMvc.perform(get("/api/files-data/" + id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(fileData.getId().toString())))
                .andExpect(jsonPath("$.extension", is(fileData.getExtension())))
                .andExpect(jsonPath("$.fileName", is(fileData.getFileName())))
                .andExpect(jsonPath("$.content", is(fileData.getContent())))
                .andExpect(jsonPath("$.sizeInKb", is(fileData.getSizeInKb())));
    }

    @Test
    @SneakyThrows
    @DisplayName(
            "Should return 'CREATED' when POST request is made to save file"
    )
    void saveSuccessful() {
        // given
        val fileData = new FileData();
        fileData.setFileName("file name");
        fileData.setExtension("jpg");
        fileData.setContent("file content");
        fileData.setSizeInKb(50);

        mockMvc.perform(post("/api/files-data/")
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(fileData))
                        .characterEncoding("utf-8"))
                .andExpect(status().isCreated())
                .andExpect(redirectedUrlPattern("/api/files-data/*"));
    }

    @Test
    @SneakyThrows
    @DisplayName(
            "Should return 'NO CONTENT' when PUT request is made to update file"
    )
    void updateFileDataSuccessful() {
        // given
        val fileData = new FileData();
        fileData.setFileName("file name");
        fileData.setExtension("jpg");
        fileData.setContent("file content");
        fileData.setSizeInKb(50);
        val id = fileDataRepository.save(fileData).getId();
        fileData.setFileName("fileName2");

        mockMvc.perform(put("/api/files-data/" + id)
                        .contentType(APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(fileData))
                        .characterEncoding("utf-8"))
                .andExpect(status().isNoContent());
        assertEquals(fileDataRepository.findById(id).get().getFileName(), fileData.getFileName());

    }

    @Test
    @SneakyThrows
    @DisplayName(
            "Should return 'NO CONTENT' when DELETE request is made to delete file"
    )
    void deleteFileDataSuccessful() {
        // given
        val fileData = new FileData();
        fileData.setFileName("file name");
        fileData.setExtension("jpg");
        fileData.setContent("file content");
        fileData.setSizeInKb(50);
        val id = fileDataRepository.save(fileData).getId();

        mockMvc.perform(delete("/api/files-data/" + id))
                .andExpect(status().isNoContent());
        assertEquals(0, fileDataRepository.findAll().size());
    }

    @AfterEach
    void tearDown() {
        fileDataRepository.deleteAll();
    }
}
