package com.sdacademy.spring_start.task10.controller;

import com.sdacademy.spring_start.SpringStartApplication;
import com.sdacademy.spring_start.task10.entity.Book;
import com.sdacademy.spring_start.task10.repository.BookRepository;
import lombok.SneakyThrows;
import lombok.val;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@SpringBootTest(classes = SpringStartApplication.class)
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(PER_CLASS)
public class BookControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    BookRepository bookRepository;

    @Test
    @SneakyThrows
    @DisplayName(
            "Should return 'OK' when fetching books by title "
    )
    void getAllByTitleSuccessful() {
        // given
        val book1 = new Book();
        book1.setTitle("title");
        book1.setAuthor("author");
        book1.setISBN("isbn");
        book1.setPagesNum(317);
        bookRepository.save(book1);
        val book2 = new Book();
        book2.setTitle("title2");
        book2.setAuthor("author2");
        book2.setISBN("isbn2");
        book2.setPagesNum(201);
        bookRepository.save(book2);

        mockMvc.perform(get("/api/books/title/title"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title", is(book1.getTitle())))
                .andExpect(jsonPath("$[0].author", is(book1.getAuthor())))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].isbn", is(book1.getISBN())))
                .andExpect(jsonPath("$[0].pagesNum", is(book1.getPagesNum())));
    }

    @Test
    @SneakyThrows
    @DisplayName(
            "Should return 'OK' when fetching books by ISBN "
    )
    void getByISBNSUccessful() {
        // given
        val book1 = new Book();
        book1.setTitle("title");
        book1.setAuthor("author");
        book1.setISBN("isbn");
        book1.setPagesNum(317);
        bookRepository.save(book1);
        val book2 = new Book();
        book2.setTitle("title2");
        book2.setAuthor("author2");
        book2.setISBN("isbn2");
        book2.setPagesNum(201);
        bookRepository.save(book2);

        mockMvc.perform(get("/api/books/isbn/isbn"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(book1.getTitle())))
                .andExpect(jsonPath("$.author", is(book1.getAuthor())))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.isbn", is(book1.getISBN())))
                .andExpect(jsonPath("$.pagesNum", is(book1.getPagesNum())));
    }

}
