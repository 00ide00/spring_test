package com.sdacademy.spring_start.task10.service;


import com.sdacademy.spring_start.task10.dto.BookDto;
import com.sdacademy.spring_start.task10.entity.Book;
import com.sdacademy.spring_start.task10.repository.BookRepository;
import com.sdacademy.spring_start.task9.exceptions.SdaException;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {

    @Mock
    BookRepository bookRepository;

    @Spy
    ModelMapper modelMapper;

    @InjectMocks
    BookService bookService;

    @Test
    void getAllByTitleSuccessful() {
        // given
        val title = "Test";
        val book1 = new Book();
        book1.setTitle(title);
        book1.setId(13L);
        book1.setAuthor("author");
        book1.setISBN("13d14");
        book1.setPagesNum(317);
        val book2 = new Book();
        book2.setTitle(title);
        book2.setId(14L);
        book2.setAuthor("author2");
        book2.setISBN("4f54j");
        book2.setPagesNum(201);
        val listOfBooks = List.of(book1, book2);
        when(bookRepository.findAllByTitle(title)).thenReturn(listOfBooks);

        // when
        val receivedListOfBooks = bookService.getAllByTitle(title);

        // then
        assertEquals(listOfBooks.get(0).getTitle(), receivedListOfBooks.get(0).getTitle());
        assertEquals(listOfBooks.get(0).getId(), receivedListOfBooks.get(0).getId());
        assertEquals(listOfBooks.get(0).getAuthor(), receivedListOfBooks.get(0).getAuthor());
        assertEquals(listOfBooks.get(0).getISBN(), receivedListOfBooks.get(0).getISBN());
        assertEquals(listOfBooks.get(0).getPagesNum(), receivedListOfBooks.get(0).getPagesNum());
        assertEquals(listOfBooks.get(1).getTitle(), receivedListOfBooks.get(1).getTitle());
        assertEquals(listOfBooks.get(1).getId(), receivedListOfBooks.get(1).getId());
        assertEquals(listOfBooks.get(1).getAuthor(), receivedListOfBooks.get(1).getAuthor());
        assertEquals(listOfBooks.get(1).getISBN(), receivedListOfBooks.get(1).getISBN());
        assertEquals(listOfBooks.get(1).getPagesNum(), receivedListOfBooks.get(1).getPagesNum());
        assertEquals(receivedListOfBooks.get(0).getClass(), BookDto.class);
        assertEquals(receivedListOfBooks.get(1).getClass(), BookDto.class);

    }


    @Test
    void getTop3ByAuthorOrderByPagesNumDescSuccessful() {
        // given
        val author = "Test";
        val book1 = new Book();
        book1.setTitle("title");
        book1.setId(13L);
        book1.setAuthor(author);
        book1.setISBN("13d14");
        book1.setPagesNum(317);
        val book2 = new Book();
        book2.setTitle("title2");
        book2.setId(14L);
        book2.setAuthor(author);
        book2.setISBN("4f54j");
        book2.setPagesNum(201);
        val listOfBooks = List.of(book1, book2);
        when(bookRepository.findTop3ByAuthorOrderByPagesNumDesc(author)).thenReturn(listOfBooks);

        // when
        val receivedListOfBooks = bookService.getTop3ByAuthorOrderByPagesNumDesc(author);

        // then
        assertEquals(listOfBooks.get(0).getTitle(), receivedListOfBooks.get(0).getTitle());
        assertEquals(listOfBooks.get(0).getId(), receivedListOfBooks.get(0).getId());
        assertEquals(listOfBooks.get(0).getAuthor(), receivedListOfBooks.get(0).getAuthor());
        assertEquals(listOfBooks.get(0).getISBN(), receivedListOfBooks.get(0).getISBN());
        assertEquals(listOfBooks.get(0).getPagesNum(), receivedListOfBooks.get(0).getPagesNum());
        assertEquals(listOfBooks.get(1).getTitle(), receivedListOfBooks.get(1).getTitle());
        assertEquals(listOfBooks.get(1).getId(), receivedListOfBooks.get(1).getId());
        assertEquals(listOfBooks.get(1).getAuthor(), receivedListOfBooks.get(1).getAuthor());
        assertEquals(listOfBooks.get(1).getISBN(), receivedListOfBooks.get(1).getISBN());
        assertEquals(listOfBooks.get(1).getPagesNum(), receivedListOfBooks.get(1).getPagesNum());
        assertEquals(receivedListOfBooks.get(0).getClass(), BookDto.class);
        assertEquals(receivedListOfBooks.get(1).getClass(), BookDto.class);

    }

    @Test
    void getByTitleStartingWithSuccessful() {
        // given
        val title = "Test";
        val book1 = new Book();
        book1.setTitle(title);
        book1.setId(13L);
        book1.setAuthor("author");
        book1.setISBN("13d14");
        book1.setPagesNum(317);
        val book2 = new Book();
        book2.setTitle(title);
        book2.setId(14L);
        book2.setAuthor("author2");
        book2.setISBN("4f54j");
        book2.setPagesNum(201);
        val listOfBooks = List.of(book1, book2);
        when(bookRepository.findByTitleStartingWith(title)).thenReturn(listOfBooks);

        // when
        val receivedListOfBooks = bookService.getByTitleStartingWith(title);

        // then
        assertEquals(listOfBooks.get(0).getTitle(), receivedListOfBooks.get(0).getTitle());
        assertEquals(listOfBooks.get(0).getId(), receivedListOfBooks.get(0).getId());
        assertEquals(listOfBooks.get(0).getAuthor(), receivedListOfBooks.get(0).getAuthor());
        assertEquals(listOfBooks.get(0).getISBN(), receivedListOfBooks.get(0).getISBN());
        assertEquals(listOfBooks.get(0).getPagesNum(), receivedListOfBooks.get(0).getPagesNum());
        assertEquals(listOfBooks.get(1).getTitle(), receivedListOfBooks.get(1).getTitle());
        assertEquals(listOfBooks.get(1).getId(), receivedListOfBooks.get(1).getId());
        assertEquals(listOfBooks.get(1).getAuthor(), receivedListOfBooks.get(1).getAuthor());
        assertEquals(listOfBooks.get(1).getISBN(), receivedListOfBooks.get(1).getISBN());
        assertEquals(listOfBooks.get(1).getPagesNum(), receivedListOfBooks.get(1).getPagesNum());
        assertEquals(receivedListOfBooks.get(0).getClass(), BookDto.class);
        assertEquals(receivedListOfBooks.get(1).getClass(), BookDto.class);

    }

    @Test
    void getAllByPagesNumBetweenSuccessful() {
        // given
        val min = 100;
        val max = 200;
        val book1 = new Book();
        book1.setTitle("title");
        book1.setId(13L);
        book1.setAuthor("author");
        book1.setISBN("13d14");
        book1.setPagesNum(317);
        val book2 = new Book();
        book2.setTitle("title2");
        book2.setId(14L);
        book2.setAuthor("author");
        book2.setISBN("4f54j");
        book2.setPagesNum(201);
        val listOfBooks = List.of(book1, book2);
        when(bookRepository.findAllByPagesNumBetween(min, max)).thenReturn(listOfBooks);

        // when
        val receivedListOfBooks = bookService.getAllByPagesNumBetween(min, max);

        // then
        assertEquals(listOfBooks.get(0).getTitle(), receivedListOfBooks.get(0).getTitle());
        assertEquals(listOfBooks.get(0).getId(), receivedListOfBooks.get(0).getId());
        assertEquals(listOfBooks.get(0).getAuthor(), receivedListOfBooks.get(0).getAuthor());
        assertEquals(listOfBooks.get(0).getISBN(), receivedListOfBooks.get(0).getISBN());
        assertEquals(listOfBooks.get(0).getPagesNum(), receivedListOfBooks.get(0).getPagesNum());
        assertEquals(listOfBooks.get(1).getTitle(), receivedListOfBooks.get(1).getTitle());
        assertEquals(listOfBooks.get(1).getId(), receivedListOfBooks.get(1).getId());
        assertEquals(listOfBooks.get(1).getAuthor(), receivedListOfBooks.get(1).getAuthor());
        assertEquals(listOfBooks.get(1).getISBN(), receivedListOfBooks.get(1).getISBN());
        assertEquals(listOfBooks.get(1).getPagesNum(), receivedListOfBooks.get(1).getPagesNum());
        assertEquals(receivedListOfBooks.get(0).getClass(), BookDto.class);
        assertEquals(receivedListOfBooks.get(1).getClass(), BookDto.class);

    }

    @Test
    void getWherePagesNumIsGreaterThanXSuccessful() {
        // given
        val min = 100;
        val book1 = new Book();
        book1.setTitle("title");
        book1.setId(13L);
        book1.setAuthor("author");
        book1.setISBN("13d14");
        book1.setPagesNum(317);
        val book2 = new Book();
        book2.setTitle("title2");
        book2.setId(14L);
        book2.setAuthor("author");
        book2.setISBN("4f54j");
        book2.setPagesNum(201);
        val listOfBooks = List.of(book1, book2);
        when(bookRepository.findWherePagesNumIsGreaterThanX(min)).thenReturn(listOfBooks);

        // when
        val receivedListOfBooks = bookService.getWherePagesNumIsGreaterThanX(min);

        // then
        assertEquals(listOfBooks.get(0).getTitle(), receivedListOfBooks.get(0).getTitle());
        assertEquals(listOfBooks.get(0).getId(), receivedListOfBooks.get(0).getId());
        assertEquals(listOfBooks.get(0).getAuthor(), receivedListOfBooks.get(0).getAuthor());
        assertEquals(listOfBooks.get(0).getISBN(), receivedListOfBooks.get(0).getISBN());
        assertEquals(listOfBooks.get(0).getPagesNum(), receivedListOfBooks.get(0).getPagesNum());
        assertEquals(listOfBooks.get(1).getTitle(), receivedListOfBooks.get(1).getTitle());
        assertEquals(listOfBooks.get(1).getId(), receivedListOfBooks.get(1).getId());
        assertEquals(listOfBooks.get(1).getAuthor(), receivedListOfBooks.get(1).getAuthor());
        assertEquals(listOfBooks.get(1).getISBN(), receivedListOfBooks.get(1).getISBN());
        assertEquals(listOfBooks.get(1).getPagesNum(), receivedListOfBooks.get(1).getPagesNum());
        assertEquals(receivedListOfBooks.get(0).getClass(), BookDto.class);
        assertEquals(receivedListOfBooks.get(1).getClass(), BookDto.class);
    }

    @Test
    void getByISBNSuccessful() {
        // given
        val isbn = "test";
        val book = new Book();
        book.setTitle("title");
        book.setId(13L);
        book.setAuthor("author");
        book.setISBN(isbn);
        book.setPagesNum(317);
        val optionalBook = Optional.of(book);
        when(bookRepository.findByISBN(isbn)).thenReturn(optionalBook);

        // when
        val receivedBook = bookService.getByISBN(isbn);

        // then
        assertEquals(receivedBook.getISBN(), book.getISBN());
        assertEquals(receivedBook.getId(), book.getId());
        assertEquals(receivedBook.getAuthor(), book.getAuthor());
        assertEquals(receivedBook.getPagesNum(), book.getPagesNum());
        assertEquals(receivedBook.getTitle(), book.getTitle());
        verify(modelMapper, times(1)).map(book, BookDto.class);


    }

    @Test
    void getByISBNWithSdaException() {
        // given
        val isbn = "random";
        Optional<Book> nullOptional = Optional.empty();
        when(bookRepository.findByISBN(isbn)).thenReturn(nullOptional);

        // when
        val sdaException = assertThrows(
                SdaException.class,
                () -> bookService.getByISBN(isbn)
        );

        // then
        assertEquals(sdaException.getMessage(), isbn + " was not found");

    }

    @Test
    void getByAuthorAndISBNSuccessful(){
        // given
        val isbn = "test";
        val author = "author";
        val book = new Book();
        book.setTitle("title");
        book.setId(13L);
        book.setAuthor(author);
        book.setISBN(isbn);
        book.setPagesNum(317);
        val optionalBook = Optional.of(book);
        when(bookRepository.findByAuthorAndISBN(author, isbn)).thenReturn(optionalBook);

        // when
        val receivedBook = bookService.getByAuthorAndISBN(author, isbn);

        // then
        assertEquals(receivedBook.getISBN(), book.getISBN());
        assertEquals(receivedBook.getId(), book.getId());
        assertEquals(receivedBook.getAuthor(), book.getAuthor());
        assertEquals(receivedBook.getTitle(), book.getTitle());
        assertEquals(receivedBook.getPagesNum(), book.getPagesNum());
        verify(modelMapper, times(1)).map(book, BookDto.class);
    }

    @Test
    void getByAuthorAndISBNWithSdaException() {
        // given
        val isbn = "random";
        val author = "author";
        Optional<Book> nullOptional = Optional.empty();
        when(bookRepository.findByAuthorAndISBN(author, isbn)).thenReturn(nullOptional);

        // when
        val sdaException = assertThrows(
                SdaException.class,
                () -> bookService.getByAuthorAndISBN(author, isbn)
        );

        // then
        assertEquals(sdaException.getMessage(), author +" with the following ISBN: "+ isbn + " was not found");
    }
}
